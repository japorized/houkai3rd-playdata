import { writable } from 'svelte/store';

export const continueId = writable('');
export const playerId = writable('');
export const nickname = writable('');
export const level = writable(1);
export const mpLevel = writable(0);
export const mpLikes = writable(0);
export const weapons = writable([]);
export const stigmataT = writable([]);
export const stigmataC = writable([]);
export const stigmataB = writable([]);
export const crystals = writable(0);
export const friends = writable([]);
export const lastAction = writable('');
export const lastTimezone = writable('');
