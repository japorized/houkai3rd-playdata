import { readable } from 'svelte/store';

export const minLevel = readable(1);
export const maxLevel = readable(85);
