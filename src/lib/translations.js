export const translations = {
  'Honkai Impact': {
    'jp': '崩壊3rd',
    'zh': '崩坏3',
    'en': 'Honkai Impact',
  },
  'Continuation ID': {
    'jp': '引き続きID',
    'zh': 'ID',
    'en': 'Continuation ID',
  },
  'Captain ID': {
    'jp': '艦長ID',
    'zh': '舰长ID',
    'en': 'Captain ID',
  },
  'Nickname': {
    'jp': 'ニックネーム',
    'zh': '译名',
    'en': 'Nickname',
  },
  'Level': {
    'jp': 'レベル',
    'zh': '等级',
    'en': 'Level',
  },
  'Multiplayer Level': {
    'jp': 'MPレベル',
    'zh': 'MP等级',
    'en': 'MP Level',
  },
  'Multiplayer Likes': {
    'jp': 'いいね',
    'zh': '赞',
    'en': 'Like',
  },
  'Valkyrie': {
    'jp': '戦乙女',
    'zh': '战乙女',
    'en': 'Valkyrie',
  },
  'Weapon': {
    'jp': '武器',
    'zh': '武器',
    'en': 'Weapon',
  },
  'Stigmata': {
    'jp': '聖痕',
    'zh': '圣痕',
    'en': 'Stigmata',
  },
  'On-hand Crystals': {
    'jp': '所持水晶数',
    'zh': '所有水晶数',
    'en': 'On-hand Crystals',
  },
  'Friends': {
    'jp': '戦友',
    'zh': '战友',
    'en': 'Friends',
  },
  'Last in-game action': {
    'jp': 'ゲーム内で最後に取った行動',
    'zh': '游戏里最后动作',
    'en': 'Last in-game action',
  },
  'Last login timezone': {
    'jp': '最後にログインした時間帯',
    'zh': '最后上线时间带',
    'en': 'Last login timezone',
  },
  'Export as JSON': {
    'jp': 'JSONとしてexport',
    'zh': '化为JSON',
    'en': 'Export as JSON',
  },
};
