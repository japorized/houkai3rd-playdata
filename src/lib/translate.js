import { lang } from '/store/preferences.js';
import { translations } from './translations.js';

export default function t(str) {
  switch (lang) {
    case 'jp':
      return translations[str].jp;
    case 'zh':
      return translations[str].zh;
    case 'en':
    default:
      return translations[str].en;
  }
}
