# Houkai3rd Play Data Record Tool (WIP)

**This is still a work in progress (WIP)**

This is meant as a tool for easily making records for your player data. 
It will have no database; `sessionStorage` will be used just to make data persist
across browser refreshes.
Users should make sure to export the finalized data to their local computer.

---

### TODO

* More UI cleanups
* Database for valkyries, weapons, and stigmata
  (only need JSON/YAML format, not SQL or the like)
* Language switching needs work
* Translation needs work

---

### Development

```
git clone https://gitlab.com/japorized/houkai3rd-playdata
cd houkai3rd-playdata
npm ci
npm run dev
```
